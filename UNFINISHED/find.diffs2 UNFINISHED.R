#Find columns in dataframe x that is most correlated to a vector y
col.blame=function(y,x){
  colclasses=sapply(x, class, simplify=FALSE)
  sel.rows=which(!is.na(y))
  #Check if y has a small number of unique values
  yclass=class(y)
  if(yclass=="factor"|yclass=="character"){
    finite.num.levels=TRUE
  }else if(yclass=="numeric"|yclass=="integer"){
    levels=unique(y[sel.rows])
    #A heuristic to see if y has a finite number of levels
    tb=table(y[sel.rows])
    if(min(tb)>10 & length(levels)<10){
      finite.num.levels=TRUE
    }else{
      finite.num.levels=FALSE
    }
  }else{
    stop("Unknown y class \"", yclass, "\"")
  }
  if(finite.num.levels){
    print("Assuming categorical y")
  }else{
    print("Assuming continuous y")
  }
  
  for(i in 1:ncol(x)){
    if(any(colclasses[[i]] %in% c("numeric", "integer")){
      #Do t-test
      
    }
  }
}