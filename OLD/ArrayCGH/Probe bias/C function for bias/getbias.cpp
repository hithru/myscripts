/*
 *  getbias.cpp
 *  
 *
 *  Created by Sudhir Varma on 3/5/2009.
 *  Copyright 2009 BSIP/OTIS/NIAID/NIH/HHS. All rights reserved.
 *
 */

using namespace std;
#include <R.h>


extern "C"{

class probe
{
	public:
		double expr;//Expression of the probe
		int indx;//Index of the probe (order along the genome)
};
class window
{
	private:
		probe *probe_list;
		int window_length;
		int lower_bound(probe p)
		{
			//Find position of probe on probe_list
			//whose expression is not lower than
			//expression of probe p
			int done=0;
			int left, right, mid;
			int pos=-1;
			left=0;
			right=num_probes-1;
			while(done==0)
			{
				mid = trunc((left + right) / 2);
				if (p.expr == probe_list[mid].expr)
				{
					pos=mid;
					done=1;
				}else if (p.expr > probe_list[mid].expr)
					left = mid + 1;
				else
					right = mid - 1;

				if(left>right)
				{
					pos=left;
					done=1;
				}
			}
			return(pos);
		}

	public:
		int num_probes;
		window(int wl)
		{
			probe_list=(probe*)R_alloc(wl, sizeof(probe));
			window_length=wl;
			num_probes=0;
		}

		~window()
		{}

		void add(probe p)
		{
			//Add the probe to the list of probes so that
			//the list remains ordered with increasing values
			//of "expr"
			int i;	
			if(num_probes==0)
			{
				probe_list[0].expr=p.expr;
				probe_list[0].indx=p.indx;
				num_probes=1;
			}else
			{
				int add_pos;
				add_pos=lower_bound(p);
				if((add_pos==num_probes) & (num_probes<window_length))
				{
					probe_list[add_pos].expr=p.expr;
					probe_list[add_pos].indx=p.indx;
					num_probes++;
				}else
				{
					//Move all probes from position add_pos to num_probes-1
					//up by one position
					//If the probe list is full (i.e. num_probes=window_length)
					//this will erase the element at position window_length-1
					for(i=num_probes-1; i>=add_pos; i--)
					{
						if(i<window_length-1)
						{
							probe_list[i+1].expr=probe_list[i].expr;
							probe_list[i+1].indx=probe_list[i].indx;
						}
					}
					//Put p at position add_pos
					probe_list[add_pos].expr=p.expr;
					probe_list[add_pos].indx=p.indx;
					if(num_probes<window_length) num_probes++;
				}
			}
		}

		void rem(probe p)
		{
			//If present, remove the probe p from probe_list
			//This removal has to be on the basis of the "indx"
			//field of the probe. But the "expr" field will also
			//match. So we find the initial position of the probe
			//in the probe_list using "expr". Then search forwards
			//and backwards to find the probe with the correct
			//"indx" field (we could have multiple probes with the
			//same "expr" field).
			int pos;
			int rem_pos;
			int done=0;
			int i=0;
			int upp_lim_reached=0, low_lim_reached=0;
			if(num_probes>0)
			{

				rem_pos=lower_bound(p);
				while((done==0) & ((upp_lim_reached==0) | (low_lim_reached==0)))
				{
					pos=rem_pos+i;
					if((pos<num_probes) & (pos>=0) & (upp_lim_reached==0))
					{
						if(probe_list[pos].indx==p.indx)
						{
							rem_pos=pos;
							done=1;
							break;
						}
					}else if(pos>=num_probes)
						upp_lim_reached=1;


					pos=rem_pos-i;
					if((pos<num_probes) & (pos>=0) & (low_lim_reached==0))
					{
						if(probe_list[pos].indx==p.indx)
						{
							rem_pos=pos;
							done=1;
							break;
						}
					}else if(pos<0)
						low_lim_reached=1;
					
					i++;
				}
				if(done==1)//The probe is present in the probe_list
				{
					for(i=rem_pos+1; i<num_probes; i++)
					{		
						probe_list[i-1].expr=probe_list[i].expr;
						probe_list[i-1].indx=probe_list[i].indx;
					}
					num_probes--;
				}
			}
		}
		double median()
		{
			div_t divresult;
			divresult = div(num_probes,2);
			int q,r;
 			q=divresult.quot;
			r=divresult.rem;
			

			if(num_probes==0)
			{
				return(-2);
			}else if(num_probes==1)
			{
				return(probe_list[0].expr);
			}else if(r==1)
			{
				return(probe_list[q].expr);
			}else if(r==0)
			{
				return((probe_list[q].expr+probe_list[q-1].expr)/2);
			}else
			{
				return(-1);
			}
		}
};



//Compute and estimate for the probe bias for
//samples using a window of length wl
void getbias(const double *X, const int *nprobes, const int *wl, double *result)
{
	int i, wr;
	window win(*wl);//Instantiate window
	probe p;

	wr=div((*wl),2).quot;//Window radius: half the window width

	//Push the first wl elements into the window
	for(i=0;i<(*wl);i++)
	{
		p.expr=X[i];
		p.indx=i;
		win.add(p);
	}
	for(i=0; i<*nprobes; i++)
	{
		if(i <= wr)
		{
			//Remove the ith element and find
			//difference between ith element and
			//median of the remaining.
			p.expr=X[i];
			p.indx=i;
			win.rem(p);
			result[i]=X[i]-win.median();
			//Put back the ith element
			win.add(p);
		}else if(i > *nprobes-wr-1)
		{
			p.expr=X[i];
			p.indx=i;
			win.rem(p);
			result[i]=X[i]-win.median();
			win.add(p);
		}else
		{
			p.expr=X[i-wr-1];
			p.indx=i-wr-1;
			win.rem(p);

			p.expr=X[i+wr];
			p.indx=i+wr;
			win.add(p);

			p.expr=X[i];
			p.indx=i;
			win.rem(p);
			result[i]=X[i]-win.median();
			win.add(p);
		}


		R_CheckUserInterrupt();//Check for interrupts in case user
		//wants to abort the computations.
	}
	

}
}/*End of Extern C*/

