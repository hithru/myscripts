/* Function to find out if a point lies within a (concave or convex) polygon */
/* Taken from http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html*/
#include <R.h>
void pnpoly(const int *nvert, const double *vertx, const double *verty, const double *testx, const double *testy, int *res)
{
  int i, j;
  *res=0;
  for (i = 0, j = *nvert-1; i < *nvert; j = i++) {
    if ( ((verty[i]> *testy) != (verty[j]> *testy)) && (*testx < (vertx[j]-vertx[i]) * (*testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
       *res = !(*res);
  }
 
}

